# Adversarial Neural Cryptography Network
# Author : Valentin urcun
import numpy as np
import tensorflow as tf
from time import strftime

from tensorflow.keras.layers import Input, Reshape, Dense, Activation, Conv1D, Flatten


#####

EPOCHS = 1000
MESS = 16
CLE = 16
BATCH  = 50
LEARNING_RATE = 0.005

#####


# fonction de perte de Bob et de Eve
def perte_BobEve(preCompletion, postCompletion):
	perte  = tf.math.abs(tf.math.subtract(preCompletion, tf.convert_to_tensor(postCompletion, np.float32)))
	perte = tf.math.reduce_sum(perte)
	perte = tf.math.reduce_mean(perte)
	return perte

# fonctipn de perte d'Alice
def perte_Alice(perteEve, perteBob):
	perteAlice = perteBob + (2*(1 - perteEve))
	return perteAlice

# détermine les bits erronés entre une solution prédite et le message d'origine
def bit_Err(sol,mess):
	solArr = tf.math.round(sol)
	diff = tf.math.abs(tf.math.subtract(solArr, tf.cast(mess, 'float32')))
	return diff

# génère une clé et un message
def genEntree ():
	# on génère le message en prenant en compte la taille du mess et le nombre de batchs
	message = np.random.randint(0,2,MESS*BATCH).reshape(BATCH, MESS)
	message = tf.convert_to_tensor(message)

	# on gènère la clé en prenant en compte la taille de la clé et le nombre de batchs
	key = np.random.randint(0,2,CLE*BATCH).reshape(BATCH, CLE)
	key = tf.convert_to_tensor(key)

	return key, message


# modèle d'Alice
def modele_Alice():

	kems = CLE + MESS
	model = tf.keras.Sequential()
	model.add(Input(shape=(kems,)))
	model.add(Dense(units=kems))
	model.add(Reshape((kems, 1)))

	model.add(Conv1D(2, 4, padding='SAME',  activation=tf.nn.sigmoid))   
	model.add(Conv1D(4, 2, padding='VALID',  activation=tf.nn.sigmoid))
	model.add(Conv1D(4, 1, padding='SAME',  activation=tf.nn.sigmoid)) 

	# sortie
	model.add(Conv1D(1, 1, padding='SAME',  activation=tf.nn.tanh))

	# on met le modèle à plat
	model.add(Flatten()) 
	# on ne garde que le message
	model.add(Dense(units=MESS)) 

	model.summary()

	return model


# modèle de Bob et d'Eve
def modele_BobEve(tailleCleMess):

	model = tf.keras.Sequential()
	model.add(Input(shape=(tailleCleMess,)))
	model.add(Dense(units=tailleCleMess))
	model.add(Reshape((tailleCleMess, 1)))

	model.add(Conv1D(2, 4, padding='SAME',  activation=tf.nn.sigmoid))   
	model.add(Conv1D(4, 2, padding='VALID',  activation=tf.nn.sigmoid))
	model.add(Conv1D(4, 1, padding='SAME',  activation=tf.nn.sigmoid)) 

	# sortie 
	model.add(Conv1D(1, 1, padding='SAME',  activation=tf.nn.tanh))

	model.add(Flatten()) 
	model.add(Dense(units=MESS)) 

	model.summary()

	return model


def main():

	alice = modele_Alice()

	# Bob connait la clé
	bob = modele_BobEve(MESS + CLE)

	# Eve ne connait pas la clé
	eve = modele_BobEve(MESS)

	# logs pour Tensorboard
	repoLogs = "logs/"+strftime("%Y-%m-%d_%H:%M:%S")
	logs = tf.summary.create_file_writer(repoLogs)

	# learning rate
	optimizer = tf.keras.optimizers.Adam(LEARNING_RATE)

	# on génère l'entrée
	cle, mess = genEntree()

	# main loop
	for epoch in range(EPOCHS):
		with tf.GradientTape(persistent = True) as tape:

			# on concatène le message avec la clé
			messCle = tf.concat([mess,cle], axis = 1)

			# alice chiffre le message
			cypher = alice(messCle, training = True)

			# on concatène le message avec la clé
			messBob = tf.concat([mess,cle], axis = 1)

			# Bob déchiffre le message
			solBob = bob(messBob, training = True)

			# Eve déchiffre le message
			solEve = eve(cypher, training = True)

			# on calcule les pertes de Bob, Eve et Alice
			perteBob = perte_BobEve(solBob, tf.cast(mess, 'float32'))
			perteEve = perte_BobEve(solEve, tf.cast(mess, 'float32'))
			perteAlice = perte_Alice(perteBob, perteEve)

			bitErrBob = tf.math.reduce_sum(bit_Err(solBob, mess)) / BATCH
			bitErrEve = tf.math.reduce_sum(bit_Err(solEve, mess)) / BATCH


		# Gradient
		gradAlice = tape.gradient(perteAlice, alice.trainable_variables)
		gradBob = tape.gradient(perteBob, bob.trainable_variables)
		gradEve = tape.gradient(perteEve, eve.trainable_variables)

		optimizer.apply_gradients(zip(gradAlice, alice.trainable_variables))
		optimizer.apply_gradients(zip(gradBob, bob.trainable_variables))
		optimizer.apply_gradients(zip(gradEve, eve.trainable_variables))

		del tape
	
		#with dirLog.as_default():
		#	tf.summary.scalar('perteBob', perte_BobEve(solBob, tf.cast(mess, 'float32')))
		#	tf.summary.scalar('perteEve', perte_BobEve(solEve, tf.cast(mess, 'float32')))
		#	tf.summary.scalar('perteAlice', perte_Alice(perteBob, perteEve)

main()
		










