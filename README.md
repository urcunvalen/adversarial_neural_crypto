# Adversarial Neural Cryptography

Dans ce programme, Alice cherche à envoyer un message chiffré à Bob.
Un agent extérieur, Eve, essaye d'intercepter le message.

Bob et Eve possèdent la même structure, mais seul Bob connaît la clé secrète.

Les stucture des trois protagonistes (Alice, Bob et Eve) ont été inspirées de solutions déjà existantes et disponibles sur internet.
# Exécution
Pour exécuter le programme, lancez d'abord la commande : 
``` Shell
pip install -r requirements.txt
```

Puis : 

```
python main.py
```

# Visualiser les logs
Les logs sont conservés dans le dossier /logs.
Pour les visualiser, tappez : 
```
tenserboard --logdir logs
```

Vous pouvez maintenant les visualiser en accédant au localhost depuis un navigateur.


